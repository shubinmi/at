all: clean proto dep test

dep:
	go mod tidy

proto:
	@if ! which protoc > /dev/null; then \
		echo "error: protoc not installed" >&2; \
		exit 1; \
	fi
	protoc --go_out=. --go_opt=paths=source_relative \
		--go-grpc_out=. --go-grpc_opt=paths=source_relative \
		./api/proto/*.proto

lint:
	golangci-lint run --fast '--tests=false'

test:
	go test ./...

clean:
	rm -rf ./api/proto/*.go

.PHONY: \
	all \
	dep \
	proto \
	test \
	lint \
	clean
