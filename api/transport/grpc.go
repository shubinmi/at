package transport

import (
	"context"
	"errors"
	"fmt"
	"net"
	"sync"

	"bitbucket.org/shubinmi/atl/internal/app/contract"

	grpcmiddleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpclogrus "github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	grpcrecovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpcctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	price "bitbucket.org/shubinmi/atl/api/proto"
	"bitbucket.org/shubinmi/atl/internal/app/controller"
	"bitbucket.org/shubinmi/atl/internal/app/errs"
)

type (
	priceService struct {
		price.UnimplementedPriceServiceServer
		ctl *controller.Price
	}
	GRPCOpt func(server *grpc.Server)
)

func (p *priceService) Fetch(ctx context.Context, req *price.FetchRequest) (*price.FetchResponse, error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "url is req")
	}
	changed, err := p.ctl.Fetch(ctx, req.Url)
	if err != nil {
		return nil, modifyCtlErr(err)
	}
	return &price.FetchResponse{ProcessedRowsCount: changed}, nil
}

func (p *priceService) List(ctx context.Context, lr *price.ListRequest) (*price.ListResponse, error) {
	if lr == nil {
		lr = &price.ListRequest{}
	}
	if lr.Pag == nil {
		lr.Pag = &price.Pagination{}
	}
	if lr.Sort == nil {
		lr.Sort = &price.Sort{}
	}
	gs, err := p.ctl.List(ctx, contract.NewFindCtrl(lr.Pag.Limit, lr.Pag.Offset, lr.Sort.FieldName, lr.Sort.Asc))
	if err != nil {
		return nil, modifyCtlErr(err)
	}
	resp := &price.ListResponse{Goods: make([]*price.Good, 0, len(gs))}
	for i := range gs {
		resp.Goods = append(resp.Goods, &price.Good{
			Id:        gs[i].ID.Hex(),
			Name:      gs[i].Name,
			Price:     gs[i].Price,
			Version:   uint64(gs[i].Version),
			UpdatedAt: uint64(gs[i].UpdatedAt),
		})
	}
	return resp, nil
}

func WithPriceService(ctl *controller.Price) GRPCOpt {
	return func(server *grpc.Server) {
		price.RegisterPriceServiceServer(server, &priceService{ctl: ctl})
	}
}

func modifyCtlErr(err error) error {
	if errors.As(err, new(errs.Invalid)) {
		return status.Errorf(codes.InvalidArgument, "bad params: %s", err.Error())
	}
	return status.Errorf(codes.Internal, "runtime: %s", err.Error())
}

func GRPSServe(ctx context.Context, wg *sync.WaitGroup, logger *logrus.Entry, address string, ops ...GRPCOpt) error {
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return err
	}
	defer listener.Close()

	logger.Logger.SetFormatter(&logrus.JSONFormatter{})
	server := grpc.NewServer(
		grpc.UnaryInterceptor(grpcmiddleware.ChainUnaryServer(
			grpcctxtags.UnaryServerInterceptor(),
			grpclogrus.UnaryServerInterceptor(logger),
			grpcrecovery.UnaryServerInterceptor(),
		)),
	)
	for i := range ops {
		ops[i](server)
	}

	wg.Add(1)
	go func() {
		<-ctx.Done()
		server.GracefulStop()
		logrus.Info("gRPC graceful shutdown done")
		wg.Done()
	}()

	logrus.Info(fmt.Sprintf("starting gRPC %s", address))
	return server.Serve(listener)
}
