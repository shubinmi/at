package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"

	"bitbucket.org/shubinmi/atl/api/transport"
	"bitbucket.org/shubinmi/atl/internal/app/controller"
	"bitbucket.org/shubinmi/atl/internal/app/usecase"
	"bitbucket.org/shubinmi/atl/internal/conf"
	"bitbucket.org/shubinmi/atl/internal/pkg/valid"
	"bitbucket.org/shubinmi/atl/internal/service/file"
	"bitbucket.org/shubinmi/atl/internal/store/mongo"
)

const (
	timeout   = time.Minute
	batchSize = 100
)

func main() {
	// init deps
	ctx, cancel := context.WithCancel(context.Background())
	wg := sync.WaitGroup{}
	appConf := conf.EnvApp()
	mongoClient, err := mongo.Client(ctx, appConf.Mongo.DSN)
	if err != nil {
		log.Fatal(err)
	}
	logger := logrus.New()
	logger.SetFormatter(&logrus.JSONFormatter{})
	defer func() {
		err = mongoClient.Disconnect(ctx)
		if err != nil {
			logger.Error(err)
		}
	}()
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGTERM)

	// init services
	priceUC := &usecase.PriceManager{
		Client: &file.Downloader{Timeout: timeout},
		CSV:    &file.Parser{BatchSize: batchSize},
		Repo:   &mongo.PriceRepo{Collection: mongoClient.Database("goods").Collection("prices")},
		Log:    logger.WithField("usecase", "price"),
	}
	priceCtl := &controller.Price{
		URLValid: valid.NewMatch(valid.URLExp),
		UseCase:  priceUC,
	}

	// init transport
	wg.Add(1)
	go func() {
		defer wg.Done()
		err = transport.GRPSServe(ctx, &wg, logger.WithField("transport", "grpc"),
			":8999",
			transport.WithPriceService(priceCtl),
		)
		if err != nil {
			logger.Error(err)
			done <- syscall.SIGTERM
		}
	}()

	<-done
	cancel()
	wg.Wait()
}
