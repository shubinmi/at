module bitbucket.org/shubinmi/atl

go 1.15

require (
	github.com/golang/protobuf v1.4.3
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.2
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/viper v1.7.1
	go.mongodb.org/mongo-driver v1.4.2
	go.uber.org/atomic v1.4.0
	golang.org/x/net v0.0.0-20201026091529-146b70c837a4 // indirect
	golang.org/x/sys v0.0.0-20201026173827-119d4633e4d1 // indirect
	google.golang.org/genproto v0.0.0-20201026171402-d4b8fe4fd877 // indirect
	google.golang.org/grpc v1.33.1
	google.golang.org/protobuf v1.25.0
)
