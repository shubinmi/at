package contract

type (
	FindCtrl struct {
		pagination struct {
			limit  uint64
			offset uint64
		}
		sort struct {
			by    string
			isAsc bool
		}
	}
)

func NewFindCtrl(limit, offset uint64, orderBy string, isAsc bool) FindCtrl {
	return FindCtrl{
		pagination: struct {
			limit  uint64
			offset uint64
		}{limit: limit, offset: offset},
		sort: struct {
			by    string
			isAsc bool
		}{by: orderBy, isAsc: isAsc},
	}
}

func (f FindCtrl) Offset() uint64 {
	return f.pagination.offset
}

func (f FindCtrl) Limit() uint64 {
	return f.pagination.limit
}

func (f FindCtrl) SetLimit(limit uint64) FindCtrl {
	f.pagination.limit = limit
	return f
}

func (f FindCtrl) By() string {
	return f.sort.by
}
func (f FindCtrl) SetBy(by string) FindCtrl {
	f.sort.by = by
	return f
}

func (f FindCtrl) IsAsc() bool {
	return f.sort.isAsc
}
