package contract

import (
	"context"
	"io"

	"bitbucket.org/shubinmi/atl/internal/store"

	"bitbucket.org/shubinmi/atl/internal/app/model"
)

type (
	NextGoods  func() ([]model.Good, error)
	Downloader interface {
		Download(ctx context.Context, url string) (io.ReadCloser, error)
	}
	CSVParser interface {
		ParsePrices(ctx context.Context, reader io.ReadCloser) (NextGoods, error)
	}
	Storage interface {
		Goods(ctx context.Context, ctrl store.FindCtrl) ([]model.Good, error)
		GoodByName(ctx context.Context, name string) (model.Good, error)
		UpdateGood(ctx context.Context, good model.Good) error
		AddGood(ctx context.Context, good model.Good) error
	}
	URLValidator interface {
		Validate(url string) error
	}
	Logger interface {
		Error(args ...interface{})
	}
)
