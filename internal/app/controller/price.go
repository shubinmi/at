package controller

import (
	"context"
	"fmt"

	"bitbucket.org/shubinmi/atl/internal/app/contract"
	"bitbucket.org/shubinmi/atl/internal/app/errs"
	"bitbucket.org/shubinmi/atl/internal/app/model"
	"bitbucket.org/shubinmi/atl/internal/app/usecase"
)

const (
	defaultLimit   = 100
	defaultOrderBy = "_id"
)

type Price struct {
	URLValid contract.URLValidator
	UseCase  *usecase.PriceManager
}

func (p *Price) List(ctx context.Context, findControls contract.FindCtrl) ([]model.Good, error) {
	if findControls.Limit() == 0 {
		findControls = findControls.SetLimit(defaultLimit)
	}
	if findControls.By() == "" {
		findControls = findControls.SetBy(defaultOrderBy)
	}
	return p.UseCase.PriceList(ctx, findControls)
}

func (p *Price) Fetch(ctx context.Context, url string) (uint64, error) {
	err := p.URLValid.Validate(url)
	if err != nil {
		return 0, fmt.Errorf("validation: %w", errs.Invalid("url"))
	}
	next, err := p.UseCase.LoadBatches(ctx, url)
	if err != nil {
		return 0, fmt.Errorf("download: %w", errs.Runtime(err.Error()))
	}
	var (
		gs           []model.Good
		ch           uint64
		totalChanged uint64
	)
LOOP:
	for {
		select {
		case <-ctx.Done():
			err = errs.Runtime("ctx killed")
			break LOOP
		default:
			gs, err = next()
			if err != nil {
				err = fmt.Errorf("load next: %w", errs.Runtime(err.Error()))
				break LOOP
			}
			if len(gs) == 0 {
				break LOOP
			}
			ch, err = p.UseCase.UpsertPrices(ctx, gs)
			if err != nil {
				err = fmt.Errorf("save prices: %w", errs.Runtime(err.Error()))
				break LOOP
			}
			totalChanged += ch
		}
	}
	return totalChanged, err
}
