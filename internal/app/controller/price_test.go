package controller

import (
	"context"
	"reflect"
	"testing"

	"bitbucket.org/shubinmi/atl/internal/app/contract"
	"bitbucket.org/shubinmi/atl/internal/app/model"
	"bitbucket.org/shubinmi/atl/internal/app/usecase"
	"bitbucket.org/shubinmi/atl/internal/store"
)

type (
	mockRepo []model.Good
)

func (m mockRepo) Goods(ctx context.Context, ctrl store.FindCtrl) ([]model.Good, error) {
	return []model.Good(m), nil
}
func (m mockRepo) GoodByName(ctx context.Context, name string) (model.Good, error) {
	panic("implement me")
}
func (m mockRepo) UpdateGood(ctx context.Context, good model.Good) error {
	panic("implement me")
}
func (m mockRepo) AddGood(ctx context.Context, good model.Good) error {
	panic("implement me")
}

func TestPrice_List(t *testing.T) {
	type fields struct {
		URLValid contract.URLValidator
		UseCase  *usecase.PriceManager
	}
	type args struct {
		ctx          context.Context
		findControls contract.FindCtrl
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []model.Good
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				URLValid: nil,
				UseCase: &usecase.PriceManager{
					Client: nil,
					CSV:    nil,
					Repo:   mockRepo{},
					Log:    nil,
				},
			},
			args: args{
				ctx:          context.Background(),
				findControls: contract.FindCtrl{},
			},
			want:    []model.Good{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Price{
				URLValid: tt.fields.URLValid,
				UseCase:  tt.fields.UseCase,
			}
			got, err := p.List(tt.args.ctx, tt.args.findControls)
			if (err != nil) != tt.wantErr {
				t.Errorf("List() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("List() got = %v, want %v", got, tt.want)
			}
		})
	}
}
