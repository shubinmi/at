package errs

import (
	"fmt"
)

type (
	Invalid  string
	Runtime  string
	NotFound string
)

func (e Invalid) Error() string {
	return fmt.Sprintf("invalid: %s", string(e))
}

func (e Runtime) Error() string {
	return fmt.Sprintf("runtime: %s", string(e))
}

func (e NotFound) Error() string {
	return fmt.Sprintf("not found: %s", string(e))
}
