package model

import (
	"fmt"
	"strconv"
	"strings"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"bitbucket.org/shubinmi/atl/internal/app/errs"
)

type Good struct {
	ID        primitive.ObjectID `json:"id" bson:"_id"`
	Name      string             `json:"name"`
	Price     uint64             `json:"price"`
	Version   uint               `json:"version"`
	UpdatedAt int64              `json:"updated_at" bson:"updated_at"`
}

func CSVBuildGood(data []string) (g Good, err error) {
	// nolint: gomnd (we wait only 2 strings currently)
	if len(data) < 2 {
		return g, errs.Invalid("wrong data count")
	}
	g.Name = data[0]
	price := data[1]
	price = strings.ReplaceAll(price, ",", ".")
	price = strings.ReplaceAll(price, " ", "")
	ps := strings.Split(price, ".")
	// nolint: gomnd (we wait max 2 part in price like $2.12)
	if len(ps) > 2 {
		return g, errs.Invalid(fmt.Sprintf("bad price %s", price))
	}
	dollars, err := strconv.ParseUint(ps[0], 10, 64)
	if err != nil {
		return Good{}, errs.Invalid(fmt.Sprintf("price dollars part must be a uint: %s; %s", data[1], err.Error()))
	}
	g.Price = dollars * 100
	// nolint: gomnd (when has no cents)
	if len(ps) == 1 {
		return g, nil
	}
	cents, err := strconv.ParseUint(ps[1], 10, 64)
	if err != nil {
		return Good{}, errs.Invalid(fmt.Sprintf("price cents part must be a uint: %s; %s", data[1], err.Error()))
	}
	g.Price += cents
	return g, nil
}
