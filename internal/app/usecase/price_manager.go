package usecase

import (
	"context"
	"errors"
	"sync"
	"time"

	"go.uber.org/atomic"

	"bitbucket.org/shubinmi/atl/internal/app/contract"
	"bitbucket.org/shubinmi/atl/internal/app/errs"
	"bitbucket.org/shubinmi/atl/internal/app/model"
	"bitbucket.org/shubinmi/atl/internal/store"
)

type (
	PriceManager struct {
		Client contract.Downloader
		CSV    contract.CSVParser
		Repo   contract.Storage
		Log    contract.Logger
	}
)

func (pl *PriceManager) PriceList(ctx context.Context, findControls contract.FindCtrl) ([]model.Good, error) {
	return pl.Repo.Goods(ctx, findControls)
}

func (pl *PriceManager) LoadBatches(ctx context.Context, url string) (contract.NextGoods, error) {
	fr, err := pl.Client.Download(ctx, url)
	if err != nil {
		return nil, err
	}
	return pl.CSV.ParsePrices(ctx, fr)
}

func (pl *PriceManager) UpsertPrices(ctx context.Context, gs []model.Good) (uint64, error) {
	wg := sync.WaitGroup{}
	changed := atomic.Uint64{}
	errCount := atomic.Uint64{}
	for i := range gs {
		i := i
		wg.Add(1)
		go func() {
			defer wg.Done()
			ch, err := pl.upsertPrice(ctx, gs[i])
			if err != nil {
				pl.Log.Error(err)
				errCount.Inc()
				return
			}
			if ch {
				changed.Inc()
			}
		}()
	}
	wg.Wait()
	if int(errCount.Load()) == len(gs) {
		return 0, errs.Runtime("nothing changed, check logs")
	}
	return changed.Load(), nil
}

func (pl *PriceManager) upsertPrice(ctx context.Context, good model.Good) (bool, error) {
	curGood, err := pl.Repo.GoodByName(ctx, good.Name)
	if err != nil && !errors.As(err, new(store.NotFoundErr)) {
		return false, err
	}
	good.Version = curGood.Version
	good.Version++
	good.UpdatedAt = time.Now().Unix()
	if curGood.Name == "" {
		err := pl.Repo.AddGood(ctx, good)
		if err != nil {
			return false, err
		}
		return true, nil
	}
	if curGood.Price == good.Price {
		return false, nil
	}
	return true, pl.Repo.UpdateGood(ctx, good)
}
