package conf

import (
	"log"

	"github.com/spf13/viper"
)

type App struct {
	Mongo struct {
		DSN string
	}
}

func EnvApp() (a App) {
	err := viper.BindEnv("MG_DSN")
	if err != nil {
		log.Println(err)
		return a
	}
	a.Mongo.DSN = viper.GetString("MG_DSN")
	return a
}
