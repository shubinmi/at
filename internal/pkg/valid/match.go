package valid

import (
	"fmt"
	"regexp"
)

const (
	URLExp = `^(?:http(s)?:\/\/).*`
)

type (
	Match struct {
		rx *regexp.Regexp
	}
	MatchErr string
)

func (e MatchErr) Error() string {
	return fmt.Sprintf("no match: %s", string(e))
}

func NewMatch(pattern string) *Match {
	return &Match{rx: regexp.MustCompile(pattern)}
}

func (u *Match) Validate(url string) error {
	if u.rx.Match([]byte(url)) {
		return nil
	}
	return MatchErr(u.rx.String())
}
