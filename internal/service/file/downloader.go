package file

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"
)

type (
	Downloader struct {
		Timeout time.Duration
	}
	DownloadErr    string
	resBodyWrapper struct {
		body io.ReadCloser
	}
)

func (r *resBodyWrapper) Read(p []byte) (n int, err error) {
	n, err = r.body.Read(p)
	if err != nil && strings.Contains(err.Error(), "http: read on closed response body") {
		return 0, io.EOF
	}
	return n, err
}

func (r *resBodyWrapper) Close() error {
	return r.body.Close()
}

func (e DownloadErr) Error() string {
	return fmt.Sprintf("download: %s", string(e))
}

func (d *Downloader) Download(ctx context.Context, url string) (io.ReadCloser, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("request: %w", DownloadErr(err.Error()))
	}
	cl := http.DefaultClient
	cl.Timeout = d.Timeout
	// nolint: bodyclose (it will happen deeper)
	rs, err := cl.Do(req)
	if err != nil {
		return nil, fmt.Errorf("do: %w", DownloadErr(err.Error()))
	}
	if rs.StatusCode >= http.StatusBadRequest {
		return nil, DownloadErr(fmt.Sprintf("bad response code %d", rs.StatusCode))
	}
	return &resBodyWrapper{body: rs.Body}, nil
}
