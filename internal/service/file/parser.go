package file

import (
	"context"
	"encoding/csv"
	"errors"
	"fmt"
	"io"

	"bitbucket.org/shubinmi/atl/internal/app/contract"
	"bitbucket.org/shubinmi/atl/internal/app/model"
)

type (
	Parser struct {
		BatchSize int
	}
	scanner struct {
		batchSize int
		frc       io.ReadCloser
		ctx       context.Context
		csRead    *csv.Reader
	}
	ParseErr string
)

func (e ParseErr) Error() string {
	return fmt.Sprintf("parse: %s", string(e))
}

func (p *Parser) ParsePrices(ctx context.Context, file io.ReadCloser) (contract.NextGoods, error) {
	s := &scanner{frc: file, ctx: ctx, csRead: csv.NewReader(file), batchSize: p.BatchSize}
	return s.nextGoods, nil
}

func (s *scanner) nextGoods() ([]model.Good, error) {
	res := make([]model.Good, 0, s.batchSize)
	for i := 0; i < s.batchSize; i++ {
		select {
		case <-s.ctx.Done():
			err := s.frc.Close()
			if err != nil {
				return nil, fmt.Errorf("close reader: %w", ParseErr(err.Error()))
			}
			return nil, ParseErr("canceled")
		default:
			row, err := s.csRead.Read()
			if err != nil {
				if errors.Is(err, io.EOF) {
					err = s.frc.Close()
					if err != nil {
						return nil, fmt.Errorf("close reader: %w", ParseErr(err.Error()))
					}
					return res, nil
				}
				return nil, fmt.Errorf("csv read: %w", ParseErr(err.Error()))
			}
			g, err := model.CSVBuildGood(row)
			if err != nil {
				return res, fmt.Errorf("build good: %w", ParseErr(err.Error()))
			}
			res = append(res, g)
		}
	}
	return res, nil
}
