package store

import "fmt"

type (
	Paginator interface {
		Offset() uint64
		Limit() uint64
	}
	Sorter interface {
		By() string
		IsAsc() bool
	}
	FindCtrl interface {
		Paginator
		Sorter
	}
	NotFoundErr string
)

func (e NotFoundErr) Error() string {
	return fmt.Sprintf("not found: %s", string(e))
}
