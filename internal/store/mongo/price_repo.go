package mongo

import (
	"context"
	"errors"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"bitbucket.org/shubinmi/atl/internal/app/model"
	"bitbucket.org/shubinmi/atl/internal/store"
)

const (
	propName    = "name"
	propPrice   = "price"
	propVersion = "version"
	propUpdated = "updated_at"
)

type PriceRepo struct {
	Collection *mongo.Collection
}

func (p *PriceRepo) Goods(ctx context.Context, ctrl store.FindCtrl) ([]model.Good, error) {
	cr, err := p.Collection.Find(ctx, bson.M{}, toOption(ctrl))
	if err != nil {
		return nil, fmt.Errorf("find: %w", err)
	}
	gs := make([]model.Good, 0)
	err = cr.All(ctx, &gs)
	if err != nil {
		return nil, fmt.Errorf("decode: %w", err)
	}
	return gs, nil
}

func toOption(ctrl store.FindCtrl) *options.FindOptions {
	if ctrl == nil {
		return nil
	}
	o := options.Find()
	if ctrl.Limit() > 0 {
		o.SetLimit(int64(ctrl.Limit()))
	}
	if ctrl.Offset() > 0 {
		o.SetSkip(int64(ctrl.Offset()))
	}
	if ctrl.By() != "" {
		direct := -1
		if ctrl.IsAsc() {
			direct = 1
		}
		o.SetSort(bson.M{ctrl.By(): direct})
	}
	return o
}

func (p *PriceRepo) GoodByName(ctx context.Context, name string) (g model.Good, err error) {
	err = p.Collection.FindOne(ctx, bson.M{propName: name}).Decode(&g)
	if err != nil && errors.Is(err, mongo.ErrNoDocuments) {
		err = store.NotFoundErr(err.Error())
	}
	return g, err
}

func (p *PriceRepo) UpdateGood(ctx context.Context, good model.Good) error {
	r, err := p.Collection.UpdateOne(
		ctx,
		bson.M{propName: good.Name},
		bson.M{"$set": bson.M{propPrice: good.Price, propVersion: good.Version, propUpdated: good.UpdatedAt}})
	if err != nil {
		return err
	}
	if r.ModifiedCount == 0 {
		return store.NotFoundErr("bad name")
	}
	return nil
}

func (p *PriceRepo) AddGood(ctx context.Context, good model.Good) error {
	_, err := p.Collection.InsertOne(ctx,
		bson.M{propName: good.Name, propPrice: good.Price, propVersion: good.Version, propUpdated: good.UpdatedAt})
	if err != nil {
		return err
	}
	return nil
}
